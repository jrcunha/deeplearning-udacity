FROM python:latest
MAINTAINER J R Cunha

# Pillow needs libjpeg by default as of 3.0.
RUN apt-get update && apt-get install -y --no-install-recommends 

RUN apt-get install -y python3
RUN apt-get install -y python3-pip
RUN pip3 install scikit-learn pyreadline Pillow
RUN pip3 install tensorflow==1.5
RUN pip3 install Jupyter
ADD ./ /app
WORKDIR /app
